'use strict';

const cron = require("node-cron");
const express = require("express");

const cronMyCuteBaby = require("./mycutebaby-scrape");

const app = express();

const PORT = process.env.PORT || 5000;

app.all('*', function(req, res, next) {
	res.header('Access-Control-Allow-Origin', '*');
	res.header('Access-Control-Allow-Methods', 'GET,POST,PUT,DELETE');
	res.header('Access-Control-Allow-Headers', 'Content-Type');
	next();
});

cron.schedule("*/15 * * * *", function() {
	cronMyCuteBaby();
});

app.get("/", function(req, res) {
	return res.send("Cron Scheduler Up !");
});

app.listen(PORT);