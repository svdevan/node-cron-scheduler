'use strict';

const puppeteer = require('puppeteer');

const cronMyCuteBaby = async function() {
	console.log("scheduler started...");
	const browser = await puppeteer.launch({
		headless: true,
		args: ['--no-sandbox', '--disable-setuid-sandbox']
	});

	const page = await browser.newPage();
	await page.goto('http://mycutebaby.in/contest/participant/?n=5e5d54f6e701e&utm_source=wsapp_share&utm_campaign=March_2020&utm_medium=shared&utm_term=wsapp_shared_5e5d54f6e701e&utm_content=participant', {
		waitUntil: 'networkidle2'
	});

	await page.waitFor('input[name=v]');
	await page.$eval('input[name=v]', el => el.value = 'Devan');

	const msg = await page.evaluate(async () => {
		document.querySelector('#vote_btn').click();
		await new Promise(function(resolve) { setTimeout(resolve, 3000) });
		return document.querySelector('#vote_msg').textContent;
	});
	console.log("State : " + msg);
	await browser.close();
	console.log("scheduler ended...");
};

module.exports = cronMyCuteBaby;